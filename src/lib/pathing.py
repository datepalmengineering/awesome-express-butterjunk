"""
    The pathing module provides a set of methods to help
    obtain path related values. Where possible paths will
    make use of contant values from /lib/constants.py to
    help make product-like name changes fairly easy to adapt.

    A set of methods to build full filepaths to interesting locations.
    Where possible these methods should use constant values (lib/constants.py)
    or programmatic lookup methods to determine the path dynamically.

    In order for pathing to be OS-agnostic during remote execution
    the test procedure checks the data for a reference to a pathing
    method. If it finds the reference, it replaces the value with a
    "pathing token". When the target machine receives the token, it
    detokenizes it into the appropriate path for the OS.
"""

import os
import sys
import posixpath
import constants
import string_extensions
from string import replace


#  PATHING TOKEN PREFIX
__PATHING_TOKEN__ = "__PATHING_TOKEN__"


def join_path_constants(*constant_list):
    """
     Joins a list of constants (from lib.constants)
     into an os-specific path. If the constant is
     a dictionary differing values for each OS, the
     proper key is selected.
    """
    ret = ""
    for constant in constant_list:
        if type(constant) is dict:
            ret = os.path.join(ret, constant[sys.platform])
        else:
            ret = os.path.join(ret, constant)
    return ret


###   PATHING METHODS
#
# TODO: Is there a way to tag these as a 'pathing method'?
# IDEA: Wrap these in a static class - safe and clean
#       but  would require modifying procedures...


def get_win_share(node):
    if sys.platform == 'linux2':
        #return "//%s\\\\c$" % string_extensions.get_host_only(node)
        return "//%s\\\\c$" % (node.ip)
    elif sys.platform == 'win32':
        #return "\\\\%s\\c$" % string_extensions.get_host_only(node)
        return "\\\\%s\\c$" % (node.ip)


def get_win_mp(node):
    if sys.platform == 'linux2':
        _p = posixpath.join(get_mountpoints_dir(),string_extensions.get_host_only(str(node)))
        return string_extensions.localize_path(_p)
    elif sys.platform == 'win32':
        return get_win_share(node)


def get_python_bin():
    if sys.platform == 'win32':
        if os.path.exists(constants.PYTHON27_WIN32):
            return constants.PYTHON27_WIN32
    elif os.path.exists(constants.PYTHON27_LINUX):
            return constants.PYTHON27_LINUX
    else:
        return "python"

def get_home_dir():
    from os.path import expanduser
    return expanduser("~")


def get_tools_bin():
    try:
        from ae import prepper
        return (os.path.abspath(os.path.join(prepper.find_ae_path(),"..","tools","bin")))
    except ImportError:
        # just in case the ae package isn't found on the initial deploy
        pass

def get_tools_src():
    try:
        from ae import prepper
        return (os.path.abspath(os.path.join(prepper.find_ae_path(),"..","tools","src")))
    except ImportError:
        # just in case the ae package isn't found on the initial deploy
        pass


def get_testfiles():
    try:
        from ae import prepper
        return (os.path.abspath(os.path.join(prepper.find_ae_path(),"testfiles")))
    except ImportError:
        # just in case the ae package isn't found on the initial deploy
        pass


def tok_it(fn_name):
    """
     Tokenizes the function name using the following scheme:

         __PATHING_METHOD_<pathing function name>

     for example: get_python_bin() would tokenize to:
         __PATHING_METHOD_get_python_bin
    """
    return"%s%s"%(__PATHING_TOKEN__,fn_name)


def is_tok(some_string):
    if isinstance(some_string, str) == False:
        return False
    if __PATHING_TOKEN__ in some_string:
        return True
    else:
        return False


def detok_it(tok):
    """
     Converts a pathing token to a function name
    """
    return tok.replace(__PATHING_TOKEN__,"")

