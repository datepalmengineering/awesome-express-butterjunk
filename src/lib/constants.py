'''
This defines static constants used throughout AE for both Linux and Windows.

This includes:

* Product Constants
* Cache config Constants
* Tool Constants
* AE2 runtime Constants
* Machine Constants
* Lab Constants

Approval should be obtained from the powers that be before any changes to constants are made.
'''

# Default procedure timeout will be set to N seconds.
# This can be overidden by the suite file or when proc runs.
DEFAULT_PROCEDURE_TIMOUT = 600

# Amount of seconds between queries to the remote proc_caller
# querying for the procedure's return value.
RPC_POLL_INTERVAL = 5

# Interval between querying a node while waiting for
# it to be up and responsive.
REBOOT_INTERVAL = 20


# OS abbreviations for the sys.platform attribute. We'll use sys
# rather than platform module to avoid the extra module import.
LIN = "linux2"
WIN = "win32"

#  --------      PRODUCT CONSTANTS      --------


#  -------       MACHINE CONSTANTS      -------
PYTHON27_LINUX = "/opt/python2.7/bin/python2.7"
PYTHON27_WIN32 = "C:\\Python27\\python.exe"
POWERSHELL_BIN = "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe"
IOGEN_TOOLS_WIN = "C:\\Wormulon\\iogen_tools"          # i/o generation tools for windows
PIP27_LINUX = "/opt/python2.7/bin/pip27"
PIP27_WIN32 = "C:\\Python27\\scripts\\pip.exe"


# -------     LAB CONSTANTS      -------
