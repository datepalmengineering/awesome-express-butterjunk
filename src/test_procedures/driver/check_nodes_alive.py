import os
import platform
import subprocess
import test_procedures as tp
from lib import string_extensions, pathing
from ae import environment, ae_errors


class CheckNodesAlive(tp.TestProcedure.TestProcedure):
    """ Test procedure that does a arbitrary ssh command to the nodes
        to verify that they are alive.
        
    """ 
    
    def action(self):
        
        # if nodes is being passed in, we set it to those
        # otherwise set it to all the nodes in the SUT
        nodes = self.args.get("nodes",self.sut.nodes)
        nodes = string_extensions.string_to_list(nodes)
     
        for node in nodes:
            if node.os.os_type == environment.OS_TYPE.LINUX:
                # assume it's an ssh command with keys already installed
                cmd = "echo foo > /dev/null" 
                cmd = "ssh %s@%s %s"%(self.sut.username, node.ip, cmd)

            elif platform.system() == 'Windows':
                 # windows -> windows so we use psexec for execution
                 cmd = "echo foo"
                 ps_exec = os.path.join(pathing.get_tools_bin(),"psTools","PsExec.exe")
                 cmd = "%s -is \\\\%s -u %s\\%s -p %s  cmd /c %s" % (ps_exec, node.ip,
                                                                    self.sut.domain,
                                                                    self.sut.username,
                                                                    self.sut.password,
                                                                    cmd)
            elif platform.system()=='Linux':
                # linux -> windows so we'll use winexe for execution
                cmd = "echo foo" 
                cmd = "(winexe --system -U %s/%s%%%s //%s \'cmd /C %s\')" %(self.sut.domain,
                                                                    self.sut.username,
                                                                    self.sut.password,
                                                                    node.ip,
                                                                    cmd)
          
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            p.wait()                                                                 
            if p.returncode != 0:
                raise ae_errors.TestProcedureError(message="Remote access to %s failed" %node)

    
    def checkpoint(self):
        pass
