import test_procedures as tp


class IdracPowerCycle(tp.TestProcedure.TestProcedure):
    """ Power cycle node using Idrac
    """ 
    
    def action(self):
        
        self.timeout = self.args.get("timeout") 
        node = self.args.get("node") 
        node_c = node.get_hostname_only()
        node_c += 'c'
        cmd = "ssh -o ConnectTimeout=%s root@%s racadm serveraction powercycle"%(self.timeout, node_c)
        self._run_shell(cmd)
    
    
    def checkpoint(self):
        pass


class IdracHardReset(tp.TestProcedure.TestProcedure):
    """ Reboot node using Idrac
    """ 
    
    def action(self):
        
        self.timeout = self.args.get("timeout") 
        node = self.args.get("node") 
        node_c = node.get_hostname_only()
        node_c += 'c'
        cmd = "ssh -o ConnectTimeout=%s root@%s racadm serveraction hardreset"%(self.timeout, node_c)
        self._run_shell(cmd)
    
    
    def checkpoint(self):
        pass

class IdracGraceShutdown(tp.TestProcedure.TestProcedure):
    """ Reboot node using Idrac
    """ 
    
    def action(self):
        
        self.timeout = self.args.get("timeout") 
        node = self.args.get("node") 
        node_c = node.get_hostname_only()
        node_c += 'c'
        cmd = "ssh -o ConnectTimeout=%s root@%s racadm serveraction graceshutdown"%(self.timeout, node_c)
        self._run_shell(cmd)
    
    
    def checkpoint(self):
        pass
