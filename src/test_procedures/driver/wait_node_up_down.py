import time
from lib import constants
import test_procedures as tp


class WaitForNodeDown(tp.TestProcedure.TestProcedure):
    """ 
        Waits for the node to go down.
        Only runs synchronous
    """
    
    def action(self):
        """
            Does an infinite ping and waits for the machine to go down.
        """
        node = self.args.get("node")
        poll = self.args.get("poll", 5)
        timeout = self.args.get("timeout", 600)

        timeout=max(timeout,200)
        ping_node = tp.sys_ops.network.PingNode(self.sut, self.suite_config)
        t=poll 

        while t < timeout:
            ping_node_pro = ping_node.run(ping_node=node)
            if ping_node_pro.shell_ret_code != 0 and ping_node_pro.shell_ret_code != None:
               self._ret_code = ping_node_pro.shell_ret_code
               break
            time.sleep(poll)
            t+=poll

        if self._ret_code == None or self._ret_code == 0:
           msg="node=[%s] did not go down" % node
           raise Exception(msg)


    def checkpoint(self):
        pass

class WaitForNodeAlive(tp.TestProcedure.TestProcedure):
    """
     Waits for the node to reboot and come back online.
     Once back up, this  is set to automatically restart
     the Pyro service on the node.
     To disable this set::

         restart_pyro = "False"

    """ 
    
    def __init__(self, env_config, suite_config):
        """ Override default timeout to 60 minutes
        """
        super(WaitForNodeAlive, self).__init__(env_config, suite_config)
        self.timeout = 60*60
    
    def action(self):
        
        node = self.args.get("node")
        restart_pyro = self.args.get("restart_pyro", "True") 
        
        # Run until the node comes back up
        tp1 = tp.driver.check_nodes_alive.CheckNodesAlive(self.sut, self.suite_config)
        while True:
            try:
                tp1.run(nodes=node)
                self.log.info("Node [%s] is alive"%node)
                # Restart the pyro service on the node
                if restart_pyro != "False":
                    try:
                        from ae import pyro_driver
                        self.log.info("Starting pyro on [%s]"%node)
                        p = pyro_driver.Pyro(self.sut)
                        p.start_proc_caller_on_node(node)
                        wait_for_pyro = 30
                        self.log.info("Waiting %s seconds for Pyro to start"%wait_for_pyro)
                        time.sleep(wait_for_pyro)
                        self.log.info("Pyro started on [%s]"%node)
                    except Exception, ex:
                        self.log.error("Error starting pyro:%s"%ex)
                        raise ex
                return
            except:
                self.log.debug("Check node alive failed. Trying again.")
                time.sleep(constants.REBOOT_INTERVAL)
            
        
    def checkpoint(self):
        pass
