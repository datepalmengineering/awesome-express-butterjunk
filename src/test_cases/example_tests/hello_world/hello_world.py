import test_procedures
from test_cases import TestCase
from ae import ae_errors
import time

class hello_world(TestCase.TestCase):
    """
    This is is a very simple test case. all it does is display a few messages.

    """
    
        
    def pre_execute(self):
        """
        There isn't anything we need to do before the main test case
        if there was, it would go here
        """
        pass     
    

    def main_execute(self):      
        """
        This requires one SUT node for the test to run.
        then this part will run on the SUT.
        """
       
        # this is assigns n1 to be the first node in the SUT's node list 
        n1 = self.sut.nodes[0]
        
        # whenever we initialize a new test procedure, we pass in the SUT and suite objects
        hello = test_procedures.example_procedures.hello_world.HelloWorld(self.sut, self.suite_config)
        my_pro = hello.run(node=n1, lic="C:\\some\\other\\path\\licensing")
        
        if not "Hello World" in my_pro.shell_stdout:
            raise ae_errors.TestCaseFail(message="Pro is incorrect:%s"%my_pro.shell_stdout)
        
        if hello.is_running() == True:
            raise ae_errors.TestCaseFail(message="Should not still be running!")
        else:
            self.log.info("Yay - it's not running.")
        
    def post_execute(self):
        """ Since this was so simple there is nothing that needs to be validated """
        pass