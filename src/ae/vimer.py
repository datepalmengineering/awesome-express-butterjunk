"""
     Classes that interact with vmware via the VIM
     to do various useful things.... and stuff.

     .. note:: This module has only been tested with a single datastore.

"""
import sys
import abc
import base64
import time
import threading
import os
import urllib2
import logging
import mmap
from urlparse import urlparse
import tempfile
from shutil import rmtree
from ae import prepper, ae_logger
from lib import network, string_extensions,constants



from pysphere import MORTypes,VIMor
from pysphere import VIServer, VIProperty
from pysphere.resources import VimService_services as VI
from pysphere.resources.vi_exception import VIException as VimErrror
from pysphere.vi_virtual_machine import VMPowerState
from pysphere.vi_task import VITask


# time to wait during VM start for the VM Tools to be online
VM_TOOLS_TIMEOUT = 300
MELLANOX_DRIVER = "Mellanox iSCSI"
ISER_PORT = 3260

class VimTool(object):
    """
     Abstract class that encapsulates the connection and cleanup tasks
     with connecting to a VMWare box.

    """
    __metaclass__ = abc.ABCMeta

    def __init__(self,sut=None,server=None,username=None,password=None):
        self.sut = sut           # the SUT
        self.server = server     # IP or hostname of vSphere server
        self.username = username # username for connection
        self.password = password # password for connection
        self._s = None           # Server connection (vSphere / ESX Host / VMGuest)
        self._trace_file = None  # vim client trace-debug log file
        self.log = None


    def __del__(self):
        try:
            self._s.disconnect()
            self._s = None
        except:
            pass


    def _set_log(self):
        """
         Sets our logger. If a sut object is passed in,
         we configure AE2 logger client otherwise use a simple logger
        """
        if self.log:
            return
        elif self.sut:
            self.log = ae_logger.Log(self.sut.log_server,self.sut.log_port,'vimer')
        else:
            self.log = logging.getLogger('vimer')


    def _set_trace_file(self):
        log_file = "%s%slogs"%(prepper.find_ae_path(),os.path.sep)
        if os.path.exists(log_file) == False:
                os.makedirs(log_file)
        log_file +="%svimer_%s.log"%(os.path.sep,
                                     string_extensions.get_host_only(network.get_local_hostname()))
        self._trace_file = log_file


    def _server_connect(self):
        """
         Connects to the vmware box we want to work with.
         This could be vSphere / ESX Host / VMGuest.
        """
        self._set_log()
        if self.username == None:
            self.username = 'Administrator'

        #TODO: acquire the ESX information from the SUT object if the server is not specified

        # if we're defaulting to root, use the default lab password
        if self.password == None and (self.username=="root" or self.username=="Administrator"):
            import base64
            self.password = base64.b64decode(constants.DEFAULT_PASSWORD)

        self._set_trace_file()
        s = VIServer()
        #self.log.debug("Connection to:%s with %s:%s"%(self.server,self.username,self.password))
        s.connect(self.server,self.username,self.password, trace_file=self._trace_file)
        self._s = s


    def get_datastore_mor(self, ds_name):
        if self._s == None:
            self._server_connect()
        datastore = [k for k,v in self._s.get_datastores().items() if v==ds_name]
        if datastore == None or len(datastore) == 0:
            raise ValueError("Failed to find datastore:%s"%ds_name)
        return datastore[0]


class OvaTool(VimTool):
    """
     Tool work working with and deploying an OVF.
    """
    def __init__(self,sut=None,server=None,username='Administrator',password=None):
        # Call the VimTool constructor
        if sut:
            super(OvaTool, self).__init__(sut=sut,
                                          server=sut.vsphere_server,
                                          username=sut.vsphere_username,
                                          password=sut.vsphere_password)
        else:
            super(OvaTool, self).__init__(server=server,username=username,password=password)

        self._host_mor = None


    def __del__(self):
        try:
            rmtree(self._ova_dir, ignore_errors=True)
            self._s.disconnect()
        except:
            pass


    def _get_ovf_from_ova(self, ova_file):
        """
         Unpacks the ova files and returns the path
         to the OVF file
        """
        import tarfile
        path = string_extensions.get_dir_from_path(ova_file)
        tf = tarfile.TarFile(ova_file)
        tf.extractall(path)
        for _file in os.listdir(path):
            if _file.endswith(".ovf"):
                return os.path.join(path,_file)


    def download_ova(self, ova_url):
        """
         Downloads the OVA from an http location and
         places it in a temporary location for us to
         unpack and set the path to the OVF.
        """
        ### TEMP so we can skip the DL and unpack ###
        #self._ovf_file = "C:\\users\\chris_~1\\appdata\\local\\temp\\tmpqlqvci_ova\\Fluid-Cache-Dell_OVF10.ovf"
        #self._ova_dir = "C:\\users\\chris_~1\\appdata\\local\\temp\\tmpqlqvci_ova"
        #return
        #####################################################################################################

        self._set_log()
        ova_file = (ova_url.split('/'))[-1]

        self._ova_dir = tempfile.mkdtemp("_ova")
        tmp_ova = os.path.join(self._ova_dir,ova_file)
        if tmp_ova[-3].lower() != ".ova":
            tmp_ova+=".ova"

        req = urllib2.urlopen(ova_url)
        self.log.debug("Starting OVA download to:%s"%tmp_ova)
        with open(tmp_ova, 'wb') as fp:
            while True:
                chunk = req.read(256 * 10240)
                if not chunk:
                    break
                fp.write(chunk)
        self.log.debug( "Download complete.")
        self._ovf_file = self._get_ovf_from_ova(tmp_ova)
        self.log.debug("OVF located at:%s"%self._ovf_file)


    def _find_network_by_name(self,network_name):
        ret = None
        for dc in  self._s.get_datacenters().keys():
            dc_properties = VIProperty(self._s, dc)
            for nw in dc_properties.network:
                if nw.name == network_name:
                    ret = nw._obj
                    break
            if ret:
                break
        if not ret:
            raise ValueError("Couldn't find network '%s'" % (network_name))
        return ret


    def _find_vmfolder_by_name(self,folder_name='vm'):
        for k,v in self._s._get_managed_objects(MORTypes.Folder).items():
            if v==folder_name:
                return k
        raise ValueError("Couldn't find folder:%s"%folder_name)


    def _find_vmdk(self):
        for _file in os.listdir(self._ova_dir):
            if _file.endswith(".vmdk"):
                return os.path.join(self._ova_dir,_file)
        raise ValueError("Failed to find vmdk file in:%s"%self._ova_dir)


    def _find_datastore(self,host,ds_name=None):
        """
         Finds the datastore Managed Object Reference (MOR). If the datatstore name is specified
         it will attempt to find it by name otherwise it will find and
         return the first datastore MOR it finds on the host.
        """
        if ds_name!=None:
            if VIMor.is_mor(ds_name):
                return ds_name
            datastore = [k for k,v in self._s.get_datastores().items() if v==ds_name]
            if datastore == None or len(datastore) == 0:
                raise ValueError("Failed to find datastore:%s"%ds_name)
            return datastore[0]

        if self.host_mor == None:
            self.host_mor = [k for k,v in self._s.get_hosts().items() if v==host][0]
        for ds_mor, name in self._s.get_datastores().items():
            props = VIProperty(self._s, ds_mor)
            host_mounts = props.host
            for hm in host_mounts:
                if self.host_mor == hm._obj.Key:
                    self.log.debug('Located datastore %s' % name)
                    return ds_mor
        raise ValueError("Failed to find datastore on %s"%host)



    def _find_resource_pool(self, host):

        dcmors = (self._s.get_datacenters()).keys()
        for dcmor in dcmors:
            dcprops = VIProperty(self._s, dcmor)
            hfmor = dcprops.hostFolder._obj
            crmors = self._s._retrieve_properties_traversal(property_names=['name','host'], from_node=hfmor, obj_type='ComputeResource')
            if not crmors:
                continue

            hostmor = [k for k,v in self._s.get_hosts().items() if v==host]
            if hostmor:
                hostmor = hostmor[0]
                self.host_mor = hostmor

            crmor = None
            for cr in crmors:
                if crmor:
                    break
                for p in cr.PropSet:
                    if p.Name == "host":
                        for h in p.Val.get_element_ManagedObjectReference():
                            if h == hostmor:
                                crmor = cr.Obj
                                break
                        if crmor:
                            break
            if crmor:
                break
        if not crmor:
            raise AttributeError("Failed to find ComputeResource MOR.")
        crprops = VIProperty(self._s, crmor)
        resource_pool = crprops.resourcePool._obj
        return resource_pool


    def _create_import_spec(self,
                           resource_pool_mor,
                           datastore_mor,
                           name,
                           host=None,
                           network_mapping=None,
                           ip_allocation_policy="fixedPolicy",
                           ip_protocol="IPv4",
                           disk_provisioning="thin"
                           ):

        # acquire the OVF file descriptor
        fh = open(self._ovf_file, "r")
        ovf_descriptor = fh.read()
        fh.close()

        #get the network MORs:
        networks = {}
        if network_mapping:
            for ovf_net_name, vmware_net_name in network_mapping.items():
                self.log.debug("net:%s v:%s"%(ovf_net_name,vmware_net_name))
                networks[ovf_net_name] = self._find_network_by_name(vmware_net_name)


        ovf_manager = self._s._do_service_content.OvfManager
        request = VI.CreateImportSpecRequestMsg()
        _this =request.new__this(ovf_manager)
        _this.set_attribute_type(ovf_manager.get_attribute_type())
        request.set_element__this(_this)
        request.set_element_ovfDescriptor(ovf_descriptor)
        rp = request.new_resourcePool(resource_pool_mor)
        rp.set_attribute_type(resource_pool_mor.get_attribute_type())
        request.set_element_resourcePool(rp)
        ds = request.new_datastore(datastore_mor)
        ds.set_attribute_type(datastore_mor.get_attribute_type())
        request.set_element_datastore(ds)
        cisp = request.new_cisp()
        cisp.set_element_entityName(name)
        cisp.set_element_locale("")
        cisp.set_element_deploymentOption("")
        if host:
            h = cisp.new_hostSystem(host)
            h.set_attribute_type(host.get_attribute_type())
            cisp.set_element_hostSystem(h)

        if networks:
            networks_map = []
            for ovf_net_name, net_mor in networks.items():
                network_mapping = cisp.new_networkMapping()
                network_mapping.set_element_name(ovf_net_name)
                n_mor = network_mapping.new_network(net_mor)
                n_mor.set_attribute_type(net_mor.get_attribute_type())
                network_mapping.set_element_network(n_mor)
                networks_map.append(network_mapping)
            cisp.set_element_networkMapping(networks_map)
        if ip_allocation_policy:
            cisp.set_element_ipAllocationPolicy(ip_allocation_policy)
        if ip_protocol:
            cisp.set_element_ipProtocol(ip_protocol)
        if disk_provisioning:
            cisp.set_element_diskProvisioning(disk_provisioning)


        request.set_element_cisp(cisp)
        return self._s._proxy.CreateImportSpec(request)._returnval

    def _import_vapp(self, resource_pool, import_spec, host=None, folder=None):
        request = VI.ImportVAppRequestMsg()
        _this =request.new__this(resource_pool)
        _this.set_attribute_type(resource_pool.get_attribute_type())
        request.set_element__this(_this)

        request.set_element_spec(import_spec.ImportSpec)

        if host:
            h = request.new_host(host)
            h.set_attribute_type(host.get_attribute_type())
            request.set_element_host(host)
        if folder:
            f = request.new_folder(folder)
            f.set_attribute_type(folder.get_attribute_type())
            request.set_element_folder(folder)
        return self._s._proxy.ImportVApp(request).Returnval


    def deploy_ovf(self, host, vapp_name, datastore=None, networks=None):
        """
         Creates  a VmWare virtual appliance on the target ESX host.

         :param host: ESX host where the virtual appliance will be deployed/
         :param vapp_name: Name of the guest Virtual appliance.
         :param datastore: Datastore where the vApp will be created. Defaults to [host - Local Disk]
         :param networks: Network mapping for the guest vApp

        **Example**::

             o = OvaTool(server='ipaddr', username='root')
             o.download_ova("url")
             o.deploy_ovf("hostname", "password")

        """

        def _keep_lease_alive(lease):
            request = VI.HttpNfcLeaseProgressRequestMsg()
            _this =request.new__this(lease)
            _this.set_attribute_type(lease.get_attribute_type())
            request.set_element__this(_this)
            request.set_element_percent(50)
            while self._keepalive:
                self._s._proxy.HttpNfcLeaseProgress(request)
                time.sleep(5)

        self._keepalive = True  # flag to keep the http connection alive

        if self._s == None:
            self._server_connect()

        folder = self._find_vmfolder_by_name()
        resource_pool = self._find_resource_pool(host)
        datastore = self._find_datastore(host, ds_name=datastore)


        # TODO: is this the correct default for VSA?
        if networks==None:
            networks = {"OVF Network Name":"VM Network"}

        self.log.debug("Creating the VM import specifications...")
        ci = self._create_import_spec(resource_pool,datastore, vapp_name, network_mapping=networks)
        if hasattr(ci,"ImportSpec") == False:
            self.log.debug("ImportSpec was malformed!")
            msg = ci.Error[0].LocalizedMessage
            self.log.debug(msg)
            raise

        self.log.debug("Import spec completed. Starting vApp deployment.")
        http_nfc_lease = self._import_vapp(resource_pool, ci, folder=folder)
        lease = VIProperty(self._s, http_nfc_lease)
        lease._flush_cache()
        while True:
            try:
                if lease.state == 'initializing':
                    time.sleep(1)
                    lease._flush_cache()
                else:
                    break
            except:
                time.sleep(1)

        if lease.state != 'ready':
            self.log.debug("Lease state not ready.")
            self.log.debug((lease._values['error']).LocalizedMessage)

        t = threading.Thread(target=_keep_lease_alive, args=(http_nfc_lease,))
        t.start()

        for dev_url in lease.info.deviceUrl:
            filename = self._find_vmdk()
            hostname = urlparse(self._s._proxy.binding.url).hostname
            upload_url = dev_url.url.replace("*", hostname)
            filename = os.path.join(self._ova_dir, filename)
            fsize = os.stat(filename).st_size
            f = open(filename,'rb')
            mmapped_file = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
            request = urllib2.Request(upload_url, mmapped_file)
            request.add_header("Content-Type", "application/x-vnd.vmware-streamVmdk")
            request.add_header("Connection", "Keep-Alive")
            request.add_header("Content-Length", str(fsize))
            opener = urllib2.build_opener(urllib2.HTTPHandler)
            resp = opener.open(request)
            mmapped_file.close()
            f.close()

        self._keepalive = False
        t.join()

        request = VI.HttpNfcLeaseCompleteRequestMsg()
        _this =request.new__this(http_nfc_lease)
        _this.set_attribute_type(http_nfc_lease.get_attribute_type())
        request.set_element__this(_this)
        self._s._proxy.HttpNfcLeaseComplete(request)

        self.log.debug("OVF deployment complete.")


class HostTool(VimTool):
    """
     Performs queries and operations pertaining ESX hosts.

     Note: We're sending all host requests thru the vSphere server
     to avoid having to disassociate the host from vSphere for management
     changes.
    """
    def __init__(self,host_name,sut=None,server=None,username='root',password=None):
        # Call the VimTool constructor
        if sut:
            super(HostTool, self).__init__(sut=sut,
                                           server=host_name,
                                           username=sut.username,
                                           password=sut.password)
        else:
            super(HostTool, self).__init__(server=server,username=username,password=password)

        self.host_name = host_name
        self._host_mor = None
        self._pci_system_id = None


    def _get_host(self):
        """
         Acquires the pySphere VIServer object
        """
        if self._s == None:
            self._server_connect()
        if self._host_mor != None:
            return self._host_mor
        else:
            for h,v in self._s.get_hosts().items():
                if v == self.host_name:
                    self._host_mor = h
                    return
        raise AttributeError("Failed to find ESX Host MOR")


    def is_vm_on_host(self, vm_name):
        """
         Checks if the virtual machine is currently in host's resource
         pool (eg.the vm is being hosted from the given ESX host).
        """
        self._get_host()

        vms = self._s._get_managed_objects(MORTypes.VirtualMachine,from_mor=self._host_mor)
        for k,v in vms.iteritems():
            if v == vm_name:
                return True
        return False


    def deploy_node(self,node):
        """
         Deploys a node to the ESX host
        """
        # massages the node-vm data and calls deploy_vm
        pass


    def deploy_vm(self, template, vm_name):
        """
         Deploys a template to specified ESX host.
        """
        pass


    def reboot(self):
        """
         Reboots the host machine
        """
        pass


    def get_ssd_devices(self):
        """
         Queries the ESX host for available SSD's.

         Returns a list of dictionaries with keys
            - pci_id      - The PCI id of the device on the host (05:00.0)
            - device_id   - The unique device id for the device as integer
            - device_name - name of the device
            - vendor_id   - the id of the manufacturer
            - vendor_name - the name of the manufacturer

        """

        #TODO: Find a device ID list to lookup
        SSD_DEV_IDS = [20816]

        self._get_host()

        properties = VIProperty(self._s, self._host_mor)
        pci_devs = properties.hardware.pciDevice
        ret = []
        for dev in pci_devs:
            #DeviceId against the list available at:http://pci-ids.ucw.cz/read/PC/
            if dev.deviceId in SSD_DEV_IDS:
                ret.append({"pci_id":dev.id,
                            "device_id":dev.deviceId,
                            "device_name":dev.deviceName,
                            "vendor_id":dev.vendorId,
                            "vendor_name":dev.vendorName,
                            })
        return ret




    def get_storage_adapters(self):
        """
        """
        self._get_host()

        properties = VIProperty(self._s, self._host_mor)
        hbas = properties.configManager.storageSystem.storageDeviceInfo.hostBusAdapter
        return hbas


    def get_iscsi_luns(self):
        self._get_host()

        ret = []
        properties = VIProperty(self._s, self._host_mor)
        luns = properties.configManager.storageSystem.storageDeviceInfo.scsiLun
        for lun in luns:
            if lun.deviceType == 'disk' and str(lun.displayName).lower().find('local') == -1:
                ret.append(lun)
        return ret


    def get_iscsi_wwns(self, luns=None):
        self._get_host()

        if not luns:
            luns = self.get_iscsi_luns()
        ret = []
        for lun in luns:
            try:
                wwn = lun.canonicalName.split('naa.')[1]
                ret.append(wwn)
            except IndexError:
                pass
        return ret


    def find_datastore_by_wwn(self, wwn):
        """
         Finds the datastore MOR residing on the disk with WWN
        """
        self._get_host()

        if not wwn:
            raise AttributeError("WWN not specified.")

        for ds_mor, name in self._s.get_datastores().items():
            props = VIProperty(self._s, ds_mor)
            host_mounts = props.host
            for hm in host_mounts:
                if self._host_mor == hm._obj.Key:
                    extents = props.info.vmfs.extent
                    for extent in extents:
                        try:
                            disk = extent.diskName.split('naa.')[1]
                            if disk == wwn:
                                return ds_mor
                        except:
                            continue


    def create_datastore(self, datastore_name, lun_wwn):
        """
            Creates a datastore on a given LUN. This will currently consume
            the entire LUN for the datastore. That is to say this does not
            support partitions.

            .. note::
                This operation has changed(?) since 5.0 and the vmware VIM API
                documentation is incredibly misleading about the operation types.
                Avoid the specs around VmfsDatastoreCreateSpec and refer to
                VmfsDatastoreOption at (
                `http://pubs.vmware.com
                <http://pubs.vmware.com/vsphere-50/topic/com.vmware.wssdk.apiref.doc_50/vim.host.VmfsDatastoreOption.html>`_
                )

                VFMS Version 5 only supports a blocksize of 1MB so that option
                is being omitted from this method.

            :param datastore_name: Human name to assign the the datastore.
            :param lun_wwn: WWN of the LUN where we want to create the datastore
        """
        self._get_host()

        VMFS_VERSION = 5

        # the canonical name is expected to begin with "naa."
        if str(lun_wwn).startswith("naa.") == False:
            lun_wwn = "naa.%s"%lun_wwn

        # create the CreateVmfsDatastoreRequestMsg request message
        request = VI.CreateVmfsDatastoreRequestMsg()

        # Find our datastore system manager MOR
        result = self._s._retrieve_properties_traversal(['configManager.datastoreSystem'],
                                                        from_node=self._host_mor,
                                                        obj_type=MORTypes.HostSystem)[0]
        for prop in result.PropSet:
            if prop.Name == 'configManager.datastoreSystem':
                hds = request.new__this(prop.Val)
                hds.set_attribute_type(MORTypes.HostDatastoreSystem)
                request.set_element__this(hds)

        spec = request.new_spec()

        # Query for the specification for creating the datastore on disk
        req = VI.QueryVmfsDatastoreCreateOptionsRequestMsg()
        req.set_element__this(hds)
        req.set_element_devicePath("/vmfs/devices/disks/%s"%lun_wwn)
        req.set_element_vmfsMajorVersion(VMFS_VERSION)
        resp = self._s._proxy.QueryVmfsDatastoreCreateOptions(req)
        try:
            spec = resp._returnval[0].Spec
        except:
            _s = "No datastore create specs found. Datastore already exists on %s?"%lun_wwn
            raise AttributeError(_s)
        spec.Vmfs.VolumeName=datastore_name
        request.set_element_spec(spec)
        # profit
        self._s._proxy.CreateVmfsDatastore(request)


    def rescan_hbas(self):
        """
         Issues an HBA rescan on the host.
        """
        self._get_host()

        request = VI.RescanAllHbaRequestMsg()
        result = self._s._retrieve_properties_traversal(['configManager.storageSystem'],
                                                        from_node=self._host_mor,
                                                        obj_type=MORTypes.HostSystem)[0]
        for prop in result.PropSet:
            if prop.Name == 'configManager.storageSystem':
                hss = request.new__this(prop.Val)
                hss.set_attribute_type(MORTypes.HostStorageSystem)
                request.set_element__this(hss)
                break

        self.log.debug('Host [%s] HBA Rescan All' % self.host_name)
        self._s._proxy.RescanAllHba(request)


    def get_iser_hba_names(self):
        """
         Queries the host for Mellanox ISER HBAs and returns a list
         of the HBA names.
        """
        self._get_host()

        iser_names = []
        hbas = self.get_storage_adapters()
        for hba in hbas:
            if hba.model.find(MELLANOX_DRIVER) > -1:
                iser_names.append(str(hba.device))
        self.log.debug("Host [%s] has ISER HBAs:%s"%(self.host_name,iser_names))
        return iser_names


    def bind_vnic(self, hba_name, vnic):
        """
         binds the virtual nic that will
         be used as an iscsi adapter

         :param hba_name: Name of the HBA
         :param vnic: Name of the virtual NIC
        """

        request = VI.BindVnicRequestMsg()
        result = self._s._retrieve_properties_traversal(['configManager.iscsiManager'],
                                                        from_node=self._host_mor,
                                                        obj_type=MORTypes.HostSystem)[0]
        for prop in result.PropSet:
            if prop.Name == 'configManager.iscsiManager':
                imgr = request.new__this(prop.Val)
                imgr.set_attribute_type(MORTypes.IscsiManager)
                request.set_element__this(imgr)
                break
        request.set_element_iScsiHbaName(hba_name)
        request.set_element_vnicDevice(vnic)
        try:
            self._s._proxy.BindVnic(request)
        except Exception, ex:
            self.log.warning("Failed to bind vNIC:%s"%ex)

    def enable_iscsi(self):
        """
         Enables software iscsi
        """
        self._get_host()

        request = VI.UpdateSoftwareInternetScsiEnabledRequestMsg()
        result = self._s._retrieve_properties_traversal(['configManager.storageSystem'],
                                                        from_node=self._host_mor,
                                                        obj_type=MORTypes.HostSystem)[0]
        for prop in result.PropSet:
            if prop.Name == 'configManager.storageSystem':
                hss = request.new__this(prop.Val)
                hss.set_attribute_type(MORTypes.HostStorageSystem)
                request.set_element__this(hss)
                break

        request.set_element_enabled("True")
        self._s._proxy.UpdateSoftwareInternetScsiEnabled(request)


    def connect_iser(self, send_targets, port=None, hba_name=None, bind_vnic=True):
        """
         Connects the iSER initiator to the target in VSA

        """
        self._get_host()

        if not send_targets:
            raise AttributeError("No send targets specified")
        if not hba_name:
            hba_name = self.get_iser_hba_names()
            if len(hba_name) == 0:
                raise AttributeError("Failed to find ISER HBA on %s"%self.host_name)
            hba_name = hba_name[0]

        if not port:
            port = ISER_PORT

        if bind_vnic:
            nics = self.find_mellanox_vnics()
            if len(nics)==0:
                raise AttributeError("Failed to find Mellanox vNICs")
            vnic = nics[0]
            self.bind_vnic(hba_name, vnic)

        ######## Skip adding the sendtargets
        request = VI.AddInternetScsiSendTargetsRequestMsg()
        result = self._s._retrieve_properties_traversal(['configManager.storageSystem'],
                                                        from_node=self._host_mor,
                                                        obj_type=MORTypes.HostSystem)[0]
        for prop in result.PropSet:
            if prop.Name == 'configManager.storageSystem':
                hss = request.new__this(prop.Val)
                hss.set_attribute_type(MORTypes.HostStorageSystem)
                request.set_element__this(hss)
                break

        request.set_element_iScsiHbaDevice(hba_name)
        t = []
        for send_target in send_targets:
            st = request.new_targets()
            st.set_element_port(port)
            st.set_element_address(send_target)
            t.append(st)
        request.set_element_targets(t)
        self._s._proxy.AddInternetScsiSendTargets(request)


    def enable_passthru(self, pci_ids):
        """
         Enables PCI passthru on the ESX host's given PCI devices.
         Note - a host reboot is required before passthru is active.

         :param pci_ids: The list of PCI device ID's to enable passthru.
        """
        self._get_host()

        request = VI.UpdatePassthruConfigRequestMsg()
        result = self._s._retrieve_properties_traversal(['configManager.pciPassthruSystem'],
                                                        from_node=self._host_mor,
                                                        obj_type=MORTypes.HostSystem)[0]

        for prop in result.PropSet:
            if prop.Name == 'configManager.pciPassthruSystem':
                pci_pts = request.new__this(prop.Val)
                pci_pts.set_attribute_type(MORTypes.HostPciPassthruSystem)
                request.set_element__this(pci_pts)
                break

        f = []
        for id in pci_ids:
            conf = request.new_config()
            conf.set_element_id(id)
            conf.set_element_passthruEnabled("True")
            f.append(conf)

        request.set_element_config(f)
        self._s._proxy.UpdatePassthruConfig(request)


    def setup_vsa_passthru(self):
        """
         High-level method that queries for all the
         appropriate PCI devices for the VSA and enables
         PCI passthru on them.
        """
        self._get_host()
        pci_ids = []

        nics = self.get_mellanox_nics()
        for nic in nics:
            pci_ids.append(nic["pci_id"])

        ssds = self.get_ssd_devices()
        for ssd in ssds:
            pci_ids.append(ssd["pci_id"])
        self.enable_passthru(pci_ids)


    def find_mellanox_vnics(self):
        """
         Finds the vnics
        """
        vSwitch_names = ["FluidCache_Network","MLNXvSwitch"]
        self._get_host()
        prop = VIProperty(self._s, self._host_mor)

        ret = []
        for vnic in prop.configManager.networkSystem.networkInfo.vnic:
            for name in vSwitch_names:
                if vnic.portgroup.find(name) > -1:
                    ret.append(vnic.device)
        return ret


    def find_mellanox_pnics(self):
        """
         Finds the mellanox NIC connected to the vSwitch.
         Returns a list of pnics associated with the host mellanox cards.
        """
        self._get_host()

        prop = VIProperty(self._s, self._host_mor)
        vSwitch_names = ["FluidCache_Network","MLNXvSwitch"]

        ret = []
        vss = []
        for _vs in prop.configManager.networkSystem.networkInfo.vswitch:
            self.log.debug("Switch: %s"%_vs.name)
            for name in vSwitch_names:
                if name in _vs.name:
                    vss.append((_vs.pnic[0],(_vs.name.split('_'))[1]))
        if len(vss) == 0:
            raise AttributeError("No Mellanox vSwitches found on %s"%self.host_name)

        for vs,ip in vss:
            for pnic in prop.configManager.networkSystem.networkInfo.pnic:
                self.log.debug("NIC:%s"%pnic.key)
                if pnic.key == vs:
                    setattr(pnic, 'ip',ip)
                    ret.append(pnic)
        if len(ret) == 0:
            raise AttributeError("No Mellanox NICs found.")
        return ret


    def get_mellanox_nics(self):
        """
         Queries the ESX host for available NICs.

         Returns a list of dictionaries with keys
             - pci_id      - The PCI id of the device on the host (05:00.0)
             - device_id   - The unique device id for the device as integer
             - device_name - name of the device
             - vendor_id   - the id of the manufacturer
             - vendor_name - the name of the manufacturer
        """
        MELLANOX_NIC_ID = 25008
        self._get_host()

        properties = VIProperty(self._s, self._host_mor)
        pci_devs = properties.hardware.pciDevice
        ret = []
        for dev in pci_devs:
            #DeviceId against the list available at:http://pci-ids.ucw.cz/read/PC/
            if dev.subDeviceId == MELLANOX_NIC_ID:
                ret.append({"pci_id":dev.id,
                            "device_id":dev.deviceId,
                            "device_name":dev.deviceName,
                            "vendor_id":dev.vendorId,
                            "vendor_name":dev.vendorName,
                            })
        return ret


    def create_vswitch(self):
        """
         Create a virtual switch on the host machine.
        """

        self._get_host()

        def add_virtual_switch(network_system, name, num_ports=120, bridge_nic=None, mtu=None):
            request = VI.AddVirtualSwitchRequestMsg()
            _this = request.new__this(network_system)
            _this.set_attribute_type(network_system.get_attribute_type())
            request.set_element__this(_this)
            request.set_element_vswitchName(name)
            spec = request.new_spec()
            spec.set_element_numPorts(num_ports)

            if isinstance(mtu, int):
                spec.set_element_mtu(mtu)
            if bridge_nic:
                bridge = VI.ns0.HostVirtualSwitchBondBridge_Def("bridge").pyclass()
                bridge.set_element_nicDevice([bridge_nic])
                spec.set_element_bridge(bridge)
            request.set_element_spec(spec)
            self._s._proxy.AddVirtualSwitch(request)


        def add_port_group(name, vlan_id, vswitch):
            request = VI.AddPortGroupRequestMsg()
            _this = request.new__this(network_system)
            _this.set_attribute_type(network_system.get_attribute_type())
            request.set_element__this(_this)
            portgrp = request.new_portgrp()

            portgrp.set_element_name('VMkernel')
            portgrp.set_element_vlanId(vlan_id)
            portgrp.set_element_vswitchName(vswitch)
            portgrp.set_element_policy(portgrp.new_policy())
            request.set_element_portgrp(portgrp)
            self._s._proxy.AddPortGroup(request)


        prop = VIProperty(self._s, self._host_mor)

        for vs in prop.configManager.networkSystem.networkInfo.vswitch:
            self.log.debug("Switch: %s"%vs.name)
        #print NIC keys
        for pnic in prop.configManager.networkSystem.networkInfo.pnic:
            self.log.debug("NIC:%s"%pnic.key)
        for pg in prop.configManager.networkSystem.networkInfo.portgroup:
            self.log.debug("Port group:%s"%pg.spec.vlanId)

        # TODO: Determine which NIC is the correct one and strip off the
        #  vmnicx key from the pnic key.
        #  eg. the pnic key is key-vim.host.PhysicalNic-vmnic1
        #   but the add virtual switch request takes vmnic1
        # TODO2: IP and subnet mask
        nic = prop.configManager.networkSystem.networkInfo.pnic[1].key
        nic = 'vmnic1'

        network_system = prop.configManager.networkSystem._obj

        vswitch_name = "Zee new switch, yarrrg"
        num_ports = 120

        add_virtual_switch(network_system, vswitch_name, num_ports, bridge_nic=nic)
        #Add a port group
        vlan_id = 0
        add_port_group(network_system, vlan_id, vswitch_name)



class VmTool(VimTool):
    """
     Wrapper class around the pySphere VIVirtualMachine object

    """
    def __init__(self,vm_name,sut=None,server=None,username=None,password=None):
        # Call the VimTool constructor
        if sut:
            super(VmTool, self).__init__(sut=sut,
                                         server=sut.vsphere_server,
                                         username=sut.vsphere_username,
                                         password=sut.vsphere_password)
        else:
            super(VmTool, self).__init__(server=server,username=username,password=password)

        self.sut = sut
        self.vm_name = vm_name
        self._vm = None
        self._authenticated = False
        self._pid = None
        self._pci_system_id = None


    def _get_vm(self):
        """
         Acquires the pySphere VIVirtualMachine object
        """
        if self._s == None:
            self._server_connect()
        if self._vm == None:
            self._vm = self._s.get_vm_by_name(self.vm_name,datacenter=None)

    def _do_authentication(self):

        if self._authenticated == True:
            return
        self._get_vm()
        if self.sut:
            self.log.debug("%s:%s"%(self.sut.username, self.sut.password))
            self._vm.login_in_guest(self.sut.username, self.sut.password)
            self._authenticated = True
        elif self.username != None:
            self.username = "root"
            self.log.debug("%s:%s"%(self.username, self.password))
            self._vm.login_in_guest(self.username,self.password)
            self._authenticated = True
        else:
            raise ValueError("No credentials to authenticate.")


    def set_mac(self, mac):

        VIRTUAL_NIC_TYPES =["VirtualE1000", "VirtualE1000e", "VirtualPCNet32",
                            "VirtualVmxnet","VirtualVmxnet3"]
        self._get_vm()

        net_device = None
        for dev in self._vm.properties.config.hardware.device:
            if dev._type in VIRTUAL_NIC_TYPES:
                net_device = dev._obj
                break
        if net_device == None:
            raise ValueError ("Failed to find virtual NIC on guest.")

        net_device.set_element_addressType("Manual")
        net_device.set_element_macAddress(mac)

        #Invoke ReconfigVM_Task
        request = VI.ReconfigVM_TaskRequestMsg()
        _this = request.new__this(self._vm._mor)
        _this.set_attribute_type(self._vm._mor.get_attribute_type())
        request.set_element__this(_this)
        spec = request.new_spec()
        dev_change = spec.new_deviceChange()
        dev_change.set_element_device(net_device)
        dev_change.set_element_operation("edit")
        spec.set_element_deviceChange([dev_change])
        request.set_element_spec(spec)
        ret = self._s._proxy.ReconfigVM_Task(request)._returnval

        #Wait for the reconfigure task to finish
        task = VITask(ret, self._s)

        status = task.wait_for_state([task.STATE_SUCCESS, task.STATE_ERROR])
        if status == task.STATE_SUCCESS:
            self.log.debug("MAC address on %s has been changed to:%s"% (self.vm_name,mac))
        else:
            raise "Error changing the MAC address:%s"%task.get_error_message()


    def _get_pci_system_id(self):

        if self._pci_system_id == None:
            self._get_vm()
            hostmor = self._vm.properties.runtime.host._obj
            dcmors = (self._s.get_datacenters()).keys()
            for dcmor in dcmors:
                dcprops = VIProperty(self._s, dcmor)
                hfmor = dcprops.hostFolder._obj
                crmors = self._s._retrieve_properties_traversal(property_names=['name','host'], from_node=hfmor, obj_type='ComputeResource')
                if not crmors:
                    continue
                crmor = None
                for cr in crmors:
                    if crmor:
                        break
                    for p in cr.PropSet:
                        if p.Name == "host":
                            for h in p.Val.get_element_ManagedObjectReference():
                                if h == hostmor:
                                    crmor = cr.Obj
                                    break
                            if crmor:
                                break
                if crmor:
                    break
            if not crmor:
                raise AttributeError("Failed to find ComputeResource mor.")

            crprops = VIProperty(self._s, crmor)
            request = VI.QueryConfigTargetRequestMsg()
            _this = request.new__this(crprops.environmentBrowser._obj)
            _this.set_attribute_type(crprops.environmentBrowser._obj.get_attribute_type ())
            request.set_element__this(_this)
            h = request.new_host(hostmor)
            h.set_attribute_type(hostmor.get_attribute_type())
            request.set_element_host(h)
            config_target = self._s._proxy.QueryConfigTarget(request)._returnval

            self._pci_system_id = config_target.PciPassthrough[0].SystemId

        return self._pci_system_id


    def add_pci(self, pci_id, dev_name, dev_id, vendor_id):
        """
          Adds (maps) the host PCI device to the guest VM.

         :param pci_id: #The name ID of this PCI ID (46:00.0).
         :param dev_name: PCI device name.
         :param dev_id: PCI device ID
         :param sys_id: host system ID
         :param vendor_id: ID if the PCI vendor
        """

        self._get_vm()

        if type(vendor_id) != int:
            raise TypeError("vendor_id must be integer value.")

        if type(dev_id) == int:
            dev_id = hex(dev_id)

        properties = VIProperty(self._s, self._vm._mor)
        if properties == None:
            raise ValueError("No properties found for guest")

        request = VI.ReconfigVM_TaskRequestMsg()
        _this = request.new__this(self._vm._mor)
        _this.set_attribute_type(self._vm._mor.get_attribute_type())
        request.set_element__this(_this)

        spec = request.new_spec()
        dc = spec.new_deviceChange()
        dc.Operation = "add"
        hd = VI.ns0.VirtualPCIPassthrough_Def("hd").pyclass()
        hd.Key = -100

        backing = VI.ns0.VirtualPCIPassthroughDeviceBackingInfo_Def("backing").pyclass()
        backing.Id = pci_id
        backing.DeviceName = dev_name
        backing.DeviceId = dev_id
        backing.SystemId = self._get_pci_system_id()
        backing.VendorId = vendor_id

        hd.Backing = backing
        dc.Device = hd
        spec.DeviceChange = [dc]
        request.Spec = spec

        task = self._s._proxy.ReconfigVM_Task(request)._returnval
        vi_task = VITask(task, self._s)
        status = vi_task.wait_for_state([vi_task.STATE_SUCCESS,
                                         vi_task.STATE_ERROR])
        if status == vi_task.STATE_SUCCESS:
            self.log.debug("PCI device %s[%s] added to %s" % (dev_name,
                                                              dev_id,
                                                              self.vm_name))
        else:
            raise AttributeError("Error adding PCI device:%s"%vi_task.get_error_message())


    def add_vsa_pcis(self):
        """
         High-level method that queries for and adds the appropriate
         PCI devices to the VSA guest machine.
        """
        self._get_vm()
        self.power_off()
        h = HostTool(self.get_esx_hostname(),sut=self.sut,server=self.server, username=self.username, password=self.password)

        nics = h.get_mellanox_nics()
        for nic in nics:
            self.add_pci(nic["pci_id"],nic["device_name"],nic["device_id"],nic["vendor_id"])

        ssds = h.get_ssd_devices()
        for ssd in ssds:
            self.add_pci(ssd["pci_id"],ssd["device_name"],ssd["device_id"],ssd["vendor_id"])
        del h


    def get_file_contents(self, remote_path, timeout=5):
        """
         Downloads a file from the VM, reads the contents and
         returns a list of the file contents.
        """
        import tempfile
        self._get_vm()

        name = tempfile.mktemp()
        i = 0
        while i < timeout:
            try:
                self._vm.get_file('/tmp/ifconfig.stdout', name)
                break
            except Exception:
                time.sleep(1)
            i+=1
        _file = open(name)
        lines = _file.readlines()
        _file.close()
        os.remove(name)
        return lines


    def enable_cache_netif(self, cache_ip, mask, pnics=None):
        """
         Takes a list of pnics objects (from HostTool.find_mellanox_pnics()),
         looks up the correct netif on guest and enables that interface.
        """
        STDOUT_FILE = "/tmp/ifconfig.stdout"
        nic = None
        ip = cache_ip
        self._get_vm()

        if pnics == None:
            h = HostTool(self.get_esx_hostname(),
                         sut=self.sut,
                         server=self.server)
            pnics = h.find_mellanox_pnics()
            del h

        cmd = "ifconfig -a > %s"%STDOUT_FILE
        self.run_shell(cmd)

        lines = self.get_file_contents(STDOUT_FILE)
        for pnic in pnics:
            for line in lines:
                # !!! Remove this after HRM-3028 (MAC offset) is resolved !!!
                #if str(pnic.mac) in line:
                if 'eth1' in line:
                    nic = line[:4]
                    #ip = pnic.ip
                    break
        if nic == None:
            raise TypeError("Failed to find Mellanox netif on %s"%(self.vm_name))

        self.enable_netif(nic, ip, mask)




    def enable_netif(self, nic, ip, mask):
        """
         Enables a network interface on the VM.
        """
        cmd = "echo -e \""
        cmd +="DEVICE=%s\\n" % nic
        cmd +="BOOTPROTO=static\\n"
        cmd +="ONBOOT=yes\\n"
        cmd +="TYPE=Ethernet\\n"
        cmd +="IPADDR=%s\\n"%ip
        cmd +="NETMASK=%s"%mask
        cmd +="\" > /etc/sysconfig/network-scripts/ifcfg-%s"%nic
        self.run_shell(cmd)

        # TODO: handle the condition where the netif is already up.
        # eth up
        cmd = "ifup %s"%nic
        self.run_shell(cmd)


    def get_esx_hostname(self):
        """
         Returns this VM's ESX host name.
        """
        self._get_vm()
        return self._vm.properties.runtime.host.name


    def power_on(self, wait_for_tools=True):
        """
         Starts the virtual machine
        """
        self._get_vm()
        if self._vm.get_status() == VMPowerState.POWERED_ON:
            return
        self._vm.power_on()
        self._vm.properties._flush_cache()
        if wait_for_tools == True:
            self._vm.wait_for_tools(timeout=VM_TOOLS_TIMEOUT)

        #there still seems to be a need for a few seconds
        time.sleep(3)
        self.log.debug("%s has been powered on."%self.vm_name)


    def power_off(self):
        """
         Stops the virtual machine
        """
        self._get_vm()
        if self._vm.get_status() == VMPowerState.POWERED_OFF:
            return
        self._vm.power_off()
        self.log.debug("%s has been powered off."%self.vm_name)


    def delete(self):
        """
         Unregisters the VM from host and removes the guest VM
         from the host disk (equivalent to 'Delete From Disk').
        """
        self._get_vm()

        self.power_off()
        # delete the vm from disk
        request = VI.Destroy_TaskRequestMsg()
        _this = request.new__this(self._vm._mor)
        _this.set_attribute_type(self._vm._mor.get_attribute_type())
        request.set_element__this(_this)
        ret = self._s._proxy.Destroy_Task(request)._returnval

        task = VITask(ret, self._s)

        status = task.wait_for_state([task.STATE_SUCCESS, task.STATE_ERROR])
        if status == task.STATE_ERROR:
            raise TypeError("Error removing vm:", task.get_error_message())
        self.log.debug("%s has been deleted."%self.vm_name)


    def migrate(self, target_host, priority='high'):
        """
         Migrates (vMotion) the VM to another host.

         :param target_host: The ESX host (host_mor) where we want to migrate the VM.
         :param priority: Task priority of the migration request.
        """
        self._get_vm()
        if not target_host:
            raise AttributeError("Migrating a VM requires a target_host.")

        # lookup the target_host's MOR if its not a MOR ID
        if VIMor.is_mor(target_host) == False:
            target_host = [k for k,v in self._s.get_hosts().items() if v==target_host][0]

        self._vm.migrate(host=target_host)


    def relocate(self, target_host, datastore, priority='default'):
        """
         relocate a VM to another host.

         :param target_host: The ESX host (host_mor) where we want to migrate the VM.
         :param datastore: The datastore MOR where the VM disks will be located.
         :param priority: Task priority of the migration request.

         **Example**::

             relocate(self, sync_run=True, priority='default', datastore=None,
             resource_pool=None, host=None, transform=None)::

        """
        self._get_vm()
        if not target_host:
            raise AttributeError("Relocating a VM requires a target_host.")
        if not datastore:
            raise AttributeError("Relocating a VM requires a datastore")

        if VIMor.is_mor(target_host) == False:
            target_host = [k for k,v in self._s.get_hosts().items() if v==target_host][0]
        if VIMor.is_mor(datastore) == False:
            datastore = self.get_datastore_mor(datastore)

        self._vm.relocate(host=target_host, datastore=datastore)


    def clone(self, target_host, datastore, new_vm_name):
        """
         Clones the virtual machine to the target host and datastore.

         :param target_host: The ESX host (host_mor) where we want to migrate the VM.
         :param datastore: The datastore MOR where the VM disks will be located.
         :param new_vm_name: The name of the new VM to be created.

        """
        self._get_vm()
        if not target_host:
            raise AttributeError("Cloning a VM requires parameter: target_host.")
        if not datastore:
            raise AttributeError("Cloning a VM requires parameter: datastore.")
        if not datastore:
            raise AttributeError("Cloning a VM requires parameter: new_vm_name.")

        if VIMor.is_mor(target_host) == False:
            target_host = [k for k,v in self._s.get_hosts().items() if v==target_host][0]
        if VIMor.is_mor(datastore) == False:
            datastore = self.get_datastore_mor(datastore)

        self._vm.clone(new_vm_name, host=target_host, datastore=datastore, power_on=False)


    def run_shell(self, cmd, cwd=None):
        """
         Runs the shell cmd on the virtual machine in a synchronous fashion
        """
        self._get_vm()
        self._do_authentication()

        # PySphere expects the args to be in a list so
        # it can use list2cmdline to escape most characters.
        #TODO: prefix commands to windows guests
        _bin = "/bin/bash"
        _cmd = ['-c']
        _cmd.append(cmd)
        self.log.debug("Executing on %s:%s"%(self.vm_name,_cmd))
        self._pid = self._vm.start_process(_bin,_cmd,cwd=cwd)

        while True:
            procs = self._vm.list_processes()
            for proc in procs:
                if proc['pid'] == self._pid:
                    if proc['exit_code'] == None:
                        time.sleep(2)
                        continue
                    elif proc['exit_code'] == 0:
                        return
                    else:
                        raise TypeError("Shell command failed:%s"%cmd)


    def kill_shell(self):
        """
         Kills the shell process that may be running on the guest.
        """
        self._get_vm()
        self._do_authentication()
        if self.pid:
            self._vm.terminate_process(self._pid)


    def run_post_ks(self):
        PREP_SCRIPT_URL = "url"
        self.log.debug("Running %s"%PREP_SCRIPT_URL)
        script = PREP_SCRIPT_URL.split('/')[-1]
        self._get_vm()
        self._do_authentication()

        ps_cmd = "yum -yq install make"
        self.run_shell(ps_cmd)

        ps_cmd = "wget %s; chmod +x ./%s; ./%s" % (PREP_SCRIPT_URL,script,script)
        self.run_shell(ps_cmd)


    def run_build_tools(self):
        BUILD_TOOLS_SCRIPT_URL = "url"
        self.log.debug("Running %s"%BUILD_TOOLS_SCRIPT_URL)
        script = BUILD_TOOLS_SCRIPT_URL.split('/')[-1]
        self._get_vm()
        self._do_authentication()

        ps_cmd = "wget %s; chmod +x ./%s; ./%s" % (BUILD_TOOLS_SCRIPT_URL,script,script)
        self.run_shell(ps_cmd)


def deploy_new_ova(vsphere,ova_url,vsa_name,host,mac,network=None,username=None,password=None):
    """
     Deploys a VSA from URL to an ESX host, configures the VSA and prepares
     it for testing tasks.
     This method is suitable when the ESX host already has been configured to
     host a VSA.

     REQUIRED PARAMETERS:

     :param vsphere: The vSphere server managing the host configuration
     :param ova_url: Some HTTP location where a pre-built OVA lives
     :param vsa_name: The name of the guest machine (VSA)
     :param host: The ESX host where the VSA will be deployed
     :param mac: MAC address of the guest machine

     OPTIONAL PARAMETERS:

     :param network: Guest machine network configuration defaults 'OVF Network Name'.
     :param username: vSphere server username (defaults to Administrator)
     :param password: vSphere server password (defaults to lab standard)
    """


    # remove the old VSA if one exists on the host
    h = HostTool(host, server=vsphere, username=username,password=password)
    if h.is_vm_on_host(vsa_name) == True:
        t = VmTool(vsa_name,server=vsphere, username=username,password=password)
        t.delete()
        del t

    o = OvaTool(server=vsphere, username=username,password=password)
    o.download_ova(ova_url)
    o.deploy_ovf(host, vsa_name)

    t = VmTool(vsa_name,server=vsphere, username=username,password=password)

    # map the PCI devices (micron and mellanox) to the guest

    t.set_mac(mac)
    t.power_on()
    time.sleep(15) # TODO: Find a way to query for idle (power state isn't accurate)
    t.run_post_ks()



def lookup_mac(hostname):
    """
    lookup hostname MAC address from dhcp confg file
    """

    DHCP_CONF = 'url'
    lines = []
    try:
        request = urllib2.urlopen(DHCP_CONF)
        lines = request.readlines()
    except:
        raise AttributeError("Failed to read %s"%DHCP_CONF)

    hostname = hostname.split('.')[0]
    for i in range(len(lines)):
        if len(lines[i].split())>2 and lines[i].split()[1] == hostname:
            return str(lines[i+1].split()[2]).rstrip(';')
    raise AttributeError("Failed to find MAC for %s"%hostname)


if __name__ == '__main__':

    usage = """

USAGE: $python vimer.py [-d][-o][-t] <vSphere server> <ESX_host_1:vsa_name, host_2:vsa_name, ...>

    Options:
    -d        : Deletes the pre-existing VSA's if they exist on the host.
    -o        : The OVA URL we want to deploy. Defaults to latest build.
    -t        : Perform extra tool installation after deployment. Includes
                Python, test tools, AE2 dependencies, etc.

"""
    """
    from optparse import OptionParser

    PREP_SCRIPT_URL = "url"
    LATEST_OVA = "url"

    ova_url = LATEST_OVA
    install_tools =False
    delete_vsa = False


    if len(sys.argv) < 3:
        print usage
        sys.exit(1)

    parser = OptionParser()
    parser.add_option("-d", "--delete_vsa",
                      default=False,
                      action="store_true",
                      dest="delete_vsa")
    parser.add_option("-o", "--ova_url",
                      type="string",
                      default=LATEST_OVA,
                      action="store",
                      dest="ova_url")
    parser.add_option("-t", "--install_tools",
                      default=False,
                      action="store_true",
                      dest="install_tools")

    vsphere = sys.argv[1]

    esx_vsas = {}
    for entry in sys.argv[2].split(','):
        host = str(entry.split(':')[0])
        if host.find('.')==-1:
            host = "%s.hostname.com"%host

        esx_vsas.update({host:entry.split(':')[1]})

    # setup a simple local logger to stdout and a file (vimer.log)
    my_logger = logging.getLogger('vimer')
    my_logger.setLevel(logging.DEBUG)
    filehandler = logging.FileHandler(filename=os.path.join(os.getcwd(),'vimer.log'))
    conhandler = logging.StreamHandler()
    formatter = logging.Formatter("%(asctime)s %(levelname)s  %(message)s")
    msg_only = logging.Formatter("%(message)s")
    filehandler.setFormatter(formatter)
    conhandler.setFormatter(msg_only)
    #conhandler.setLevel(logging.INFO)
    my_logger.addHandler(filehandler)
    my_logger.addHandler(conhandler)


    o = OvaTool(server=vsphere, username='Administrator')
    my_logger.info("Downloading %s"%ova_url)
    o.download_ova(ova_url)


    if delete_vsa==True:
        for host,vsa in esx_vsas.items():
            print host
            print vsa
            h = HostTool(host, server=vsphere, username='Administrator')
            vm_name = "FluidCache_VSA_%s_%s"%(host,vsa)
            if h.is_vm_on_host(vm_name) == True:
                my_logger.info("Deleting VM %s"%vm_name)
                t = VmTool(vm_name, server=vsphere, username='Administrator')
                t.delete()
                del t
            else:
                my_logger.info("Did not find VSA to delete %s"%vm_name)
            del h


    for host,vsa in esx_vsas.items():
        vm_name = "FluidCache_VSA_%s_%s"%(host,vsa)
        my_logger.info("Deploying OVA to %s"%host)
        o.deploy_ovf(host, vm_name)

        #configure the guest (mac and pci devs)
        t = VmTool(vm_name,server=vsphere, username='Administrator')
        t.set_mac(lookup_mac(host))
        t.add_vsa_pcis()
        my_logger.info("Powering on %s"%vsa)
        t.power_on()

        # removes the old host keys from the driver
        #rm = test_procedures.sys_ops.os_util.RemoveKnownHostKeys(self.sut,self.suite_config)

        if install_tools == True:
            my_logger.info("Installing tools...")
            t.run_shell("yum -qy install zlib-devel")
            t.run_shell("yum -qy install openssl-devel")
            t.run_shell("yum -qy install python-devel")
            t.run_shell("yum -qy install libaio-devel")
            t.run_post_ks()
            t.run_shell("yum -qy install libxml2-devel")
            t.run_shell("yum -qy install libxslt-devel")
            t.run_shell("yum -qy install rsync")
            t.run_shell("yum -qy install ntpdate")

    exit(0)
    """

    print "Starting to clone..."

    VSPHERE="vsphere.hostname.com"
    VM = "vm_backup"

    #         (     ESX host,                 datastore,      VM name),
    targets = [
               ("hostname", "datastore", "hostname"),
               ("hostname", "datastore", "hostname"),
              ]

    source_vm = VmTool(VM, server=VSPHERE, username="username", password="password")
    i = 1
    for target_host, datastore, new_vm_name in targets:
        print "%s Cloning to %s %s %s" % (i, target_host, datastore, new_vm_name)
        source_vm.clone(target_host, datastore, new_vm_name)

        print "    Setting the MAC address for %s" % new_vm_name
        try:
            _macaddy = lookup_mac(new_vm_name)
            _temp = VmTool(new_vm_name, server=VSPHERE, username="username", password="password")
            _temp.set_mac(_macaddy)
            _temp.power_on(wait_for_tools=False)
            print "    Powererd on."
            del _temp
        except AttributeError:
            print "!!! Failed to find MAC address for %s"%new_vm_name
        i+=1

    print "All done."
